// document.querySelector('.list').addEventListener('click', function(e){
//     console.log(e.target)
// })

// e.stopPropagation() and e.preventDefault
// document.querySelector('.item--pink').addEventListener('click', function(e){
//     e.preventDefault() // this is...
//     console.log(e.target.value)
//     e.stopPropagation()// this is...
// })

// remove elem
document.querySelector('.list').addEventListener('click', function(e){
    //var removeTarget = e.target.parentNode
    // removeTarget.parentNode.removeChild()
    console.log(e.target)
    e.target.remove()
    
}, false)
// how add the last elem that I've just removed? 
// let btn = document.querySelector('button')
// btn.addEventListener('click', function(e) {
//     e.target.add()
// })

// 1)--- CHANGE COLOR --- 
const h1 = document.querySelector('.heading-primary')
h1.style.color = 'orange'

// 2)--- CREATING ELEMENTS ---
const ul = document.querySelector('.list1')
const li = document.createElement('li')
ul.append(li)

// 3) Add CLASS or ID and modifying it
li.setAttribute('class', 'my-item') // li.className = 'my_item'
li.removeAttribute('class')
li.innerText = 'Hi man, you will achieve an outstanding results' // simple text
// li.textContent = 'Hi, you will achieve an outstanding results' // text with gaps
// li.innerHTML = 'Hi, you will achieve an outstanding results' // text with tags, z.B. <span>

